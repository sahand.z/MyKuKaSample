package com.mykuka.mykukasampleapp

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker


class MapActivity: AppCompatActivity() {
    private lateinit var mapController: IMapController
    private lateinit var mapView: MapView
    private lateinit var confirmBtn: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        init()
    }

    private fun init() {
        initMap()
        confirmBtn = findViewById(R.id.mapCnfirrmBtn)
        confirmBtn.setOnClickListener({
            finish()
        })
    }

    private fun initMap() {
        mapView = findViewById(R.id.mapview)
        Configuration.getInstance().setUserAgentValue(getApplicationContext().getPackageName())
        mapView.setTileSource(TileSourceFactory.MAPNIK)
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        mapController = mapView.getController()
        mapView.setMultiTouchControls(true)
        mapController.setZoom(15.0)
        val loc = GeoPoint( 35.7676325,51.3192201)
        mapController.setCenter(loc)
    }
}