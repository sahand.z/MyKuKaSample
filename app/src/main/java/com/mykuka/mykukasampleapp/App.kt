package com.mykuka.mykukasampleapp

import android.app.Application
import com.mykuka.mykukasampleapp.di.KoinModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            val km = KoinModules()
            androidLogger()
            androidContext(this@App)
            modules(arrayOf(km.mainModule,km.repoModule,km.retrofitModule,km.schedulerModule).toList())
        }
    }
}