package com.mykuka.mykukasampleapp.di

import com.mykuka.mykukasampleapp.Constants
import com.mykuka.mykukasampleapp.main.model.MainReepoImple
import com.mykuka.mykukasampleapp.main.model.MainRepo
import com.mykuka.mykukasampleapp.main.p.MainPresentor
import com.mykuka.mykukasampleapp.main.p.MainPresentorImple
import com.mykuka.mykukasampleapp.main.v.MainView
import com.mykuka.mykukasampleapp.model.ServiceUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class KoinModules {

    val retrofitModule = module {

        single {
            okHttp()
        }
        single {
            retrofit(Constants.BASE_URL)
        }
        single {
            get<Retrofit>().create(ServiceUtil::class.java)
        }
    }
    val mainModule = module {
        single<MainPresentor> {
            (view: MainView) -> MainPresentorImple(get(named("ioScheduler")),get(named("androidSchuler")),get(),view)
        }
    }

    val schedulerModule = module {
        single<Scheduler>(named("ioScheduler")){
            Schedulers.io()
    }
        single<Scheduler>(named("androidSchuler")){
            AndroidSchedulers.mainThread()
    }

    }

    val repoModule = module {
        single<MainRepo> {
            MainReepoImple(androidContext())
        }
    }

    private fun okHttp() = OkHttpClient.Builder()
        .build()


    private fun retrofit(baseUrl: String) = Retrofit.Builder()
        .callFactory(OkHttpClient.Builder().build())
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

}

