package com.mykuka.mykukasampleapp.main.v

import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel

interface MainView {
    fun setProductsList(it: List<ProductModel>)
    fun setFeatuedProductsImg(it: List<ProductModel>)
    fun setNews(it: List<NewsModel>)
    fun goToMapActivity()
}