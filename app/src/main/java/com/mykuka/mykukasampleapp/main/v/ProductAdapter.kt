package com.mykuka.mykukasampleapp.main.v

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mykuka.mykukasampleapp.R
import com.mykuka.mykukasampleapp.model.pojo.ProductModel

class ProductAdapter(val context: Activity, var list: List<ProductModel>, val onClickListener: View.OnClickListener): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_product,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fill(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val productIMG: ImageView = itemView.findViewById(R.id.itemProductsImg)
        val productTxt: TextView = itemView.findViewById(R.id.itemProductTxt)

        fun fill(model: ProductModel) {
            itemView.setOnClickListener(onClickListener)
            productIMG.setImageDrawable(ContextCompat.getDrawable(context,model.imgId))
            productTxt.setText(model.name)
        }

    }
}
