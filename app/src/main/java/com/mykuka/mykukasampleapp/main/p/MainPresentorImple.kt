package com.mykuka.mykukasampleapp.main.p

import com.mykuka.mykukasampleapp.main.model.MainRepo
import com.mykuka.mykukasampleapp.main.v.MainView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

class MainPresentorImple(
    val ioSchuler: Scheduler,
    val androidSchedulers: Scheduler,
    val repo: MainRepo,
    val view: MainView
) : MainPresentor {

    override fun getProducts() {
        repo.getProducts()
            .subscribeOn(ioSchuler)
            .observeOn(androidSchedulers)
            .subscribe(
                {
                    view.setProductsList(it)
                }, {

                }
            )

    }

    override fun getNews() {
        repo.getNews()
            .subscribeOn(ioSchuler)
            .observeOn(androidSchedulers)
            .subscribe({
                view.setNews(it)
            }, {

            }
            )

    }

    override fun geteFeaturedProduct() {
        repo.getFeaturedProduct()
            .subscribeOn(ioSchuler)
            .observeOn(androidSchedulers)
            .subscribe({
                view.setFeatuedProductsImg(it)
            }, {

            }
            )

    }

    override fun productClick() {
        view.goToMapActivity()
    }
}