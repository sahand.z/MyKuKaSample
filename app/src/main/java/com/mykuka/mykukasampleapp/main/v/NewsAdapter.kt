package com.mykuka.mykukasampleapp.main.v

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mykuka.mykukasampleapp.R
import com.mykuka.mykukasampleapp.model.pojo.NewsModel

class NewsAdapter(val context: Context,var list: List<NewsModel> = arrayListOf()): RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.newsItemImg)
        val title: TextView = itemView.findViewById(R.id.newsItemTitleTxt)
        val subTitle: TextView = itemView.findViewById(R.id.newsItemSubtitleTxt)
        fun fill(item: NewsModel) {
            imageView.setImageDrawable(ContextCompat.getDrawable(context,item.imgId))
            title.setText(item.title)
            subTitle.setText(item.subtitle)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_news,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fill(list.get(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }

}
