package com.mykuka.mykukasampleapp.main.p

interface MainPresentor {
    fun getProducts()
    fun getNews()
    fun geteFeaturedProduct()
    fun productClick()
}