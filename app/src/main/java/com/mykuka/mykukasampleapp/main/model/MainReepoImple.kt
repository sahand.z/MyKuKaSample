package com.mykuka.mykukasampleapp.main.model

import android.content.Context
import com.mykuka.mykukasampleapp.R
import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel
import io.reactivex.rxjava3.core.Single

class MainReepoImple(val context: Context): MainRepo {

    override fun getFeaturedProduct(): Single<List<ProductModel>> {
        var list : List<ProductModel> = arrayOf(ProductModel("Cleaning",R.drawable.p2_icon),
                ProductModel("Shopping",R.drawable.p8_icon),
                ProductModel("Massage",R.drawable.p25_icon),
        ).toList()
        return Single.just(list)
    }

    override fun getNews(): Single<List<NewsModel>> {
        var list : List<NewsModel> = arrayOf(NewsModel(R.drawable.news_1, "How to Use The App" , "Getting Access On Demand"),
                NewsModel(
                    R.drawable.news_2,
                    "List Your Service On Mykuka",
                    "Do You Offer Manpowe"
                )).toList()
        return Single.just(list)
    }

    override fun getProducts(): Single<List<ProductModel>> {
        var list : List<ProductModel> = arrayOf(ProductModel("Cleaning",R.drawable.p2_icon),
                ProductModel("Event Assistant",R.drawable.p3_icon),
                ProductModel("Office Assistant",R.drawable.p4_icon),
                ProductModel("Coffee Delivery",R.drawable.p6_icon),
                ProductModel("Food Delivery",R.drawable.p7_icon),
                ProductModel("Shopping Delivery",R.drawable.p8_icon),
                ProductModel("Grocery Delivery",R.drawable.p9_icon),
                ProductModel("Messanger Delivery",R.drawable.p10_icon),
                ProductModel("Bills Payment",R.drawable.p12_icon),
                ProductModel("Personal Assistant",R.drawable.p13_icon),
                ProductModel("Assistant on Bi...",R.drawable.p13_icon),
                ProductModel("Queuing up",R.drawable.p15_icon),
                ProductModel("Pet Sitting",R.drawable.p16_icon),
                ProductModel("Flyeing",R.drawable.p17_icon),
                ProductModel("Dish Washing",R.drawable.p23_icon),
                ProductModel("Cash Washing",R.drawable.p24_icon),
                ProductModel("Massage",R.drawable.p25_icon),
                ProductModel("Deep Clean",R.drawable.p26_icon),
                ProductModel("Car Wash and...",R.drawable.p27_icon),
                ProductModel("Monicure and Pe...",R.drawable.p28_icon),

        )
                .toList()
        return Single.just(list)
    }

}