package com.mykuka.mykukasampleapp.main.v

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mykuka.mykukasampleapp.MapActivity
import com.mykuka.mykukasampleapp.R
import com.mykuka.mykukasampleapp.main.p.MainPresentor
import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf


class MainActivity : AppCompatActivity(), MainView {
    val presentor: MainPresentor by inject(){
        parametersOf(this)
    }

    lateinit var recyclerProducts: RecyclerView
    lateinit var newsRecycler: RecyclerView
    lateinit var productsAdapter: ProductAdapter
    lateinit var newsAdapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    override fun onResume() {
        super.onResume()
        presentor.getProducts()
        presentor.getNews()

    }
    private fun init() {
        newsRecycler = findViewById(R.id.newsRecycler)
        recyclerProducts = findViewById(R.id.productsRecycler)
        initRecyclerProducts()
        initRecyclerNews()
    }

    private fun initRecyclerProducts() {
        val layoutManager = GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false)
        recyclerProducts.layoutManager = layoutManager
        productsAdapter=  ProductAdapter(this, arrayListOf(),{
            presentor.productClick()
        })
        recyclerProducts.adapter = productsAdapter
    }

    private fun initRecyclerNews() {
        val layoutManager = LinearLayoutManager(this,  LinearLayoutManager.HORIZONTAL, false)
        newsRecycler.layoutManager = layoutManager
        newsAdapter =  NewsAdapter(this, arrayListOf())
        newsRecycler.adapter = newsAdapter

    }

    override fun setProductsList(it: List<ProductModel>) {
        productsAdapter.list = it
    }

    override fun setFeatuedProductsImg(it: List<ProductModel>) {

    }

    override fun setNews(it: List<NewsModel>) {
        newsAdapter.list = it

    }

    override fun goToMapActivity() {
        startActivity(Intent(MainActivity@this,MapActivity::class.java))
    }
}