package com.mykuka.mykukasampleapp.main.model

import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel
import io.reactivex.rxjava3.core.Single

interface MainRepo {
    fun getProducts(): Single<List<ProductModel>>

    fun getNews(): Single<List<NewsModel>>

    fun getFeaturedProduct():Single<List<ProductModel>>

}