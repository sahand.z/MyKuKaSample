package com.mykuka.mykukasampleapp.model.pojo

import android.graphics.drawable.Drawable

data class ProductModel(val name: String, val imgId: Int)
