package com.mykuka.mykukasampleapp.model.pojo

import android.graphics.drawable.Drawable

data class NewsModel(val imgId: Int, val title: String, val subtitle: String)
