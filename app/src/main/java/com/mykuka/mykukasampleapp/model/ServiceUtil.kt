package com.mykuka.mykukasampleapp.model

import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel
import io.reactivex.Single
import retrofit2.http.GET

interface ServiceUtil {

    @GET
    fun getProducts(): Single<List<ProductModel>>

    @GET
    fun getFeaturedProducts(): Single<List<ProductModel>>

    @GET
    fun getNews(): Single<List<NewsModel>>
}
