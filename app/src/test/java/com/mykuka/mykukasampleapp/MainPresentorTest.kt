package com.mykuka.mykukasampleapp

import com.mykuka.mykukasampleapp.main.model.MainRepo
import com.mykuka.mykukasampleapp.main.p.MainPresentorImple
import com.mykuka.mykukasampleapp.main.v.MainView
import com.mykuka.mykukasampleapp.model.pojo.NewsModel
import com.mykuka.mykukasampleapp.model.pojo.ProductModel
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.unmockkAll
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import java.util.concurrent.TimeUnit

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MainPresentorTest {


    lateinit var ts: TestScheduler
    @Before
    fun before(){
        MockKAnnotations.init(this)
    }

    @After
    fun after(){
        unmockkAll()
    }


    @RelaxedMockK
    lateinit var repo: MainRepo

    @RelaxedMockK
    lateinit var mockedView: MainView
    fun createPresentor(view: MainView = mockedView) = MainPresentorImple(ts,ts,repo,view)

    @Test
    fun `when news updated, view gets updated news`() {
        val newsList: List<NewsModel> = arrayOf(
        NewsModel(1," ", " ")).toList()
        every { repo.getNews() } returns Single.just(newsList)
        val viewStub =  ViewStub()
        ts = TestScheduler()
        ts.advanceTimeBy(3000, TimeUnit.MILLISECONDS)
        ts.triggerActions()
        val presentor = createPresentor(viewStub)
        repo.getNews().subscribeOn(ts).subscribe({
            presentor.getNews()

            assertEquals(newsList,viewStub.newslst)

        },{

        })
    }
    @Test
    fun `when product updated, view gets updated products`() {
        val productList: List<ProductModel> = arrayOf(
        ProductModel("laundary",1)).toList()
        every { repo.getProducts() } returns Single.just(productList)
        val viewStub =  ViewStub()
        ts = TestScheduler()
        ts.advanceTimeBy(3000, TimeUnit.MILLISECONDS)
        ts.triggerActions()
        val presentor = createPresentor(viewStub)
        repo.getProducts().subscribeOn(ts).subscribe({
            presentor.getProducts()

            assertEquals(productList,viewStub.products)

        },{

        })
    }

    @Test
    fun `when product get clicked, view must go to map activity`() {
        val viewStub =  ViewStub()
        ts = TestScheduler()
        ts.advanceTimeBy(3000, TimeUnit.MILLISECONDS)
        ts.triggerActions()
        val presentor = createPresentor(viewStub)
        presentor.productClick()
        assertEquals(true,viewStub.goToNext)
    }

    class ViewStub: MainView {
        lateinit var products: List<ProductModel>
        lateinit var newslst: List<NewsModel>
        var goToNext = false
        override fun setProductsList(it: List<ProductModel>) {
            this.products = it
        }

        override fun setFeatuedProductsImg(it: List<ProductModel>) {
            TODO("Not yet implemented")
        }

        override fun setNews(it: List<NewsModel>) {
            newslst = it
        }

        override fun goToMapActivity() {
            goToNext = true
        }

    }


}